/*
  plotstl.c
  A small example to demonstrate use of EricDraw library
  Generates a PNG with a few geometrical objects on it.

  Copyright (c) 2010, Eric Shalov.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of the <organization> nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ericdraw.h"

struct point {
  double x,y,z;
};

struct triangle {
  struct point point[3];
};

int allocated_triangles = 0;
struct triangle *triangle;
int num_triangles = 0;

/* prototypes */
void load_stl(char *stl_filename);

int main(int argc, char *argv[]) {
     char *output_filename = NULL;
     struct image *i;
     
     char *stl_filename = NULL;
     
     double min_x, min_y, min_z, max_x, max_y,max_z, mid_x, mid_y, mid_z;
     double fx1,fy1,fz1,fx2,fy2,fz2;
     double scale;
     double rot_x,rot_y,rot_z; /* yup, in radians. 2*PI radians make a circle */
     int x1,y1,x2,y2;
     int p, t;
     
     if(argc != 5) {
       fprintf(stderr,"Usage: %s [filename.stl] [degrees yaw] [degrees pitch] [degrees roll]\n", argv[0]);
       return -1;
     }

     stl_filename = argv[1];
     asprintf(&output_filename, "%s.png", stl_filename);
     load_stl(stl_filename);     

     /* Desired Rotation */
     rot_x =  strtod(argv[4],NULL) / 180.0 * M_PI; /* roll left */
     rot_y =  strtod(argv[2],NULL) / 180.0 * M_PI; /* yaw right */
     rot_z =  strtod(argv[3],NULL) / 180.0 * M_PI; /* pitch down */


     /* Determine range of X, Y, and Z points in model */
     min_x = max_x = triangle[0].point[0].x;
     min_y = max_y = triangle[0].point[0].y;
     min_z = max_z = triangle[0].point[0].z;
     for(t=0;t<num_triangles;t++) {
       for(p=0;p<3;p++)
         if(triangle[t].point[p].x < min_x) min_x = triangle[t].point[p].x;
         if(triangle[t].point[p].y < min_y) min_y = triangle[t].point[p].y;
         if(triangle[t].point[p].z < min_z) min_z = triangle[t].point[p].z;
         if(triangle[t].point[p].x > max_x) max_x = triangle[t].point[p].x;
         if(triangle[t].point[p].y > max_y) max_y = triangle[t].point[p].y;
         if(triangle[t].point[p].z > max_z) max_z = triangle[t].point[p].z;
     }

     /* Determine center of object */
     mid_x = (max_x + min_x) / 2.0;
     mid_y = (max_y + min_y) / 2.0;
     mid_z = (max_z + min_z) / 2.0;

     printf("Ranges: X = %.2lf .. %.2lf,  Y = %.2lf .. %.2lf,  Z = %.2lf .. %.2lf\n",
       min_x, max_x,  min_y, max_y,  min_z, max_z);
     printf("Midpoint: %.2lf,%.2lf,%.2lf\n", mid_x, mid_y, mid_z);

     /* Create image palette */     
     i = draw_create_image(300,300);
     draw_full(i, white);
     draw_color(i, blue);
     draw_textf(i, 5, 5, 1, "Yaw:   %6.1f", 180.0 * rot_y / M_PI);
     draw_textf(i, 5,15, 1, "Pitch: %6.1f", 180.0 * rot_z / M_PI);
     draw_textf(i, 5,25, 1, "Roll:  %6.1f", 180.0 * rot_x / M_PI);
     draw_textf(i, 5,i->height-8*4-3, 2, "Plot of \"%s\"", stl_filename);
     draw_textf(i, 5,i->height-8*2-1, 2, "%d triangles", num_triangles);

     draw_color(i, black);


     for(t=0;t<num_triangles;t++) {
       for(p=0;p<3;p++) {

         /* Choose a scalign factor based on the most extent dimension */         
         scale = max_x - min_x;
         if( (max_y - min_y) > scale ) scale = max_y - min_y;
         if( (max_z - min_z) > scale ) scale = max_z - min_z;

         fx1 = (triangle[t].point[p].x       - mid_x) / scale;
         fy1 = (triangle[t].point[p].y       - mid_y) / scale;
         fz1 = (triangle[t].point[p].z       - mid_z) / scale;
         fx2 = (triangle[t].point[(p+1)%3].x - mid_x) / scale;
         fy2 = (triangle[t].point[(p+1)%3].y - mid_y) / scale;
         fz2 = (triangle[t].point[(p+1)%3].z - mid_z) / scale;

         /* Rotate model around it's center */
  
         fx1 = fx1 * cos(rot_z) - fy1 * sin(rot_z);
         fy1 = fx1 * sin(rot_z) + fy1 * cos(rot_z);
         fx1 = fx1 * cos(rot_y) - fz1 * sin(rot_y);
         fz1 = fx1 * sin(rot_y) + fz1 * cos(rot_y);
         fy1 = fy1 * cos(rot_x) - fz1 * sin(rot_x);
         fz1 = fy1 * sin(rot_x) + fz1 * cos(rot_x);

         fx2 = fx2 * cos(rot_z) - fy2 * sin(rot_z);
         fy2 = fx2 * sin(rot_z) + fy2 * cos(rot_z);
         fx2 = fx2 * cos(rot_y) - fz2 * sin(rot_y);
         fz2 = fx2 * sin(rot_y) + fz2 * cos(rot_y);
         fy2 = fy2 * cos(rot_x) - fz2 * sin(rot_x);
         fz2 = fy2 * sin(rot_x) + fz2 * cos(rot_x);


         x1 = i->width/2  + (fx1 * i->width) * 0.99;
         y1 = i->height/2 - (fy1 * i->width) * 0.99;
         x2 = i->width/2  + (fx2 * i->width) * 0.99;
         y2 = i->height/2 - (fy2 * i->width) * 0.99;

         /*printf("%d,%d, %d,%d\n", x1,y1,x2,y2);*/

         draw_line(i, x1,y1, x2,y2);
       }
       
       if(0) printf("%d: %d,%d (%.2lf,%.2lf,%.2lf) - %d,%d (%.2lf,%.2lf,%.2lf)\n",
         t,
         x1,y1,
         triangle[t].point[0].x,
         triangle[t].point[0].y,
         triangle[t].point[0].z,
         
         x2,y2,
         triangle[t].point[1].x,
         triangle[t].point[1].y,
         triangle[t].point[1].z
       );
       

       /*0,0, i->width-1, i->height-1);*/
     }

     draw_write_png(i, output_filename);

     draw_free_image(i);
     
     free(output_filename);
     free(triangle);

     return 0;
}


void load_stl(char *stl_filename) {
     FILE *stl_file = NULL;
     char line[128], *vertex_begins = NULL;
     int line_number = 0;
     int num_points;
     double x,y,z;
     
     if( ! (stl_file = fopen(stl_filename,"r")) ) {
       perror("fopen");
       exit(EXIT_FAILURE);
     }

     while( fgets(line,128,stl_file) ) {
       ++line_number;
       
       if(strstr(line,"outer loop")) {
         ++num_triangles;
         num_points=0;
         
         if(num_triangles > allocated_triangles) {
           allocated_triangles += 4000; /* when we need to realloc(), let's give it a big chunk */
           triangle = realloc(triangle, allocated_triangles * sizeof(struct triangle));
           if(!triangle) {
             fprintf(stderr,"Out of memory after reading in %d triangles!\n", num_triangles);
             exit(EXIT_FAILURE);
           }
         }
       }
       
       else if((vertex_begins=strstr(line,"vertex"))) {
         if( sscanf(vertex_begins,"vertex %lf %lf %lf", &x,&y,&z) == 3) {
             triangle[num_triangles-1].point[num_points].x = x;
             triangle[num_triangles-1].point[num_points].y = y;
             triangle[num_triangles-1].point[num_points].z = z;
             ++num_points;
         } else {
           printf("Error reading vertex on line %d: \"%s\"", line_number, line);
         }
       }
     }
     printf("%d triangles loaded.\n", num_triangles);

     fclose(stl_file);
}
