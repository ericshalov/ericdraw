<?php header("Content-type: text/html; charset=utf-8"); ?>
<? echo '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' . "\n" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
     <title>EricDraw - A simple and tiny C drawing/painting library</title>
     <style type="text/css">
       #top_images {
         clear: both;
       }
       #top_images img {
         float: left;
         margin-bottom: 2em;
       }
       
       #examples {
         clear: both;
       }
     
       #examples li {
         clear: both;
       }
       #examples img {
       float:right; margin: 0 0 15px 15px;
       }
     
       li {
         margin-top: 1em;
         margin-bottom: 0em;
       }
       
       #fine_print {
         font-size: 0.75em;
       }
     </style>

    <script type="javascript">
    index = 0;

    function rotate() {
      --index;
      if(index == -1) index = 9;
      
      document.getElementById("cessna").src ="cessna-" + index + ".png";
      
      t = setTimeout("rotate()", 500);
    }
    </script>
</head>

<body onload="rotate();">
  <div id="intro">
     <h1>EricDraw - A simple and tiny C (beta) drawing/painting library</h1>

     <p>
     Intended as a much less-sophisticated replacement for libraries like
     libgd, for applications where a tiny graphics library is useful (for
     low-footprint CGI graphics generation, or in embedded development). 
     </p>
     
     <p>
     Requires libjpeg, libtiff and libpng.
     </p>
  </div>
  
  <div id="top_images">
     <p>
     <img src="N33W119.jpg"  width="232" height="232" alt="Elevation Model" />
     <img src="example7.png" width="232" height="232" alt="Geometry" />
     <img src="cessna2.png"  width="232" height="232" alt="Cessna 182" id="cessna" />
     <img src="example6.png" width="232" height="232" alt="Epitrochoid" />
     </p>
  </div>
  
  <div id="examples">
     <h2>Examples:</h2>
     <br />
     <ul>

     <li><img src="example1.png" width="320" height="240" alt="Example 1" />
     Example 1: Drawing some lines and other stuff.<br />
     You can draw lines, circles, boxes, text. You can flood-fill (paint-bucket) areas.
     </li>

     <li><img src="example2.png" width="256" height="128" alt="Example 2" />
     Example 2: Drawing text<br />
     A 7-bit ASCII font is included with the library. 
     </li>

     <li><img src="example3.png" width="448" height="288" alt="Example 3" />
     Example 3: Text scaling<br />
     The built-in rasterized font (8x8 pixels) can be scaled to add basic labels to your images.
     </li>

     <li><img src="example4.jpg" width="320" height="16"  alt="Example 4" />
     Example 4: Deterring spammers<br />
     Deter spammers that use "screen-scraping" automated spiders to collect
     e-mail addresses off of websites. Encode e-mail addresses as images to
     make the task more difficult for them, and convolute the images (making them "captchas").
     </li>
     
     <li><img src="eyes.jpg" width="182" height="62" alt="Example 5" />
     Example 5: Cropping an image<br />
     The <i>draw_create_image_by_cropping()</i> function can be used to
     create a new image by cropping down an existant image canvas.
     </li>
     </ul>
  </div>
  
  <div id="downloads">
     <h2>Downloads:</h2>
     <ul>
          <li>Download the package: <a href="ericdraw-1.2.tgz">ericdraw-1.2.tgz</a> [85 kbytes]</li>
     </ul>
  </div>
  
  <div id="quick_start">
     <h2>Quickstart:</h2>
     <ol>
     <li>Include the library header:<br />
     <pre>
     #include "ericdraw.h"
     </pre>
     </li>

     <li>Declare a pointer to act as the handle for the image canvas:<br />
     <pre>
     struct image *i;
     </pre>
     </li>

     <li>Create an image canvas <i>i</i> in memory with <i>draw_create_image(width,height)</i><br />
     <pre>
     i = draw_create_image(320,240);
     </pre>
     </li>

     <li>Draw on the image with <i>draw_line()</i>, etc..<br />
     <pre>
     draw_color(i, black);
     draw_line(i, 160, 30,  120,210);
     draw_line(i, 160, 30,  200,210);
     draw_line(i,  80,120,  240,120);
     draw_box(i, 60,20, 260,220);
     draw_circle(i, 160,120, 70);
     </pre>
     </li>
     
     <li>Write the image to disk with <i>draw_write_png()</i><br />
     <pre>
     draw_write_png(i, "myimage.png");
     </pre>
     </li>
     
     <li>Clean-up by calling <i>draw_free_image()</i><br />
     <pre>
     draw_free_image(i);
     </pre>
     </li>
          
     <li>Link against the EricDraw library, libjpeg, libtiff, and libpng (you may also need the math library):<br />
     $ <b>gcc -o <i>myprogram</i> <i>myprogram.c</i> -L. -lericdraw -lm -ljpeg -lpng -ltiff</b>
     </li>
     </ol>
  </div>
  
  <div id="reference">
     <h2>Function Reference:</h2>

     <ul>
     <li><i>struct image *draw_create_image(int width, int height);</i><br />
     Allocate (dynamically) an image of a given size. This must be done before
     calling any drawing functions. Use the image canvas handle that it returns
     in all future calls.  Don't forget to call <i>draw_free_image()</i> later.
     </li>

     <li><i>struct image *draw_create_image_by_cropping(struct image *i, int x1,int y1,int x2,int y2);</i><br />
     Create a new image canvas by cropping an existing one. The original image
     remains unperturbed, and thus both the original image and the new one will
     need to be de-allocated with <i>draw_free_image()</i>.
     </li>

     <li><i>struct image *draw_create_image_from_tiff(char *filename);</i><br />
     Create a new image by loading in a TIFF file from disk. Don't forget to call <i>draw_free_image()</i> later.
     </li>

     <li><i>struct image *draw_create_image_from_png(char *filename);</i><br />
     Create a new image by loading in a PNG file from disk. Don't forget to call <i>draw_free_image()</i> later.
     </li>

     <li><i>struct image *draw_create_image_from_jpeg(char *filename);</i><br />
     Create a new image by loading in a JPEG file from disk. Don't forget to call <i>draw_free_image()</i> later.
     </li>
     
     <li><i>void draw_color(struct image *i, struct color color);</i><br />
     Set the current drawing (pen) color.
     </li>

     <li><i>void draw_background_color(struct image *i, struct color color);</i><br />
     Set the current background color for text.
     </li>

     <li><i>void draw_pixel(struct image *i, int x, int y, struct color color);</i><br />
     Set a single pixel to a particular color.
     </li>

     <li><i>void draw_text(struct image *i, int x, int y, int scale, char *text);</i><br />
     Draw 7-bit ASCII text using a simple built-in raster font.
     </li>

     <li><i>void draw_textf(struct image *i, int x, int y, int scale, char *format, ...);</i><br />
     Same as draw_text, but allows printf()-style formatting of any desired output.
     </li>

     <li><i>void draw_fill_block(struct image *i, int x1,int y1,int x2,int y2, struct color color);</i><br />
     Fill a blocks solidly in the specified color.
     </li>

     <li><i>void draw_full(struct image *i, struct color color);</i><br />
     Fill the ENTIRE image with a specified color (a good way to initialize the image.)
     </li>

     <li><i>void draw_line(struct image *i, int x1, int y1, int x2, int y2);</i><br />
     Draw a 2D line.
     </li>

     <li><i>void draw_circle(struct image *i, int x, int y, double radius);</i><br />
     Draw a circle.
     </li>

     <li><i>void draw_box(struct image *i, int x1, int y1, int x2, int y2);</i><br />
     Draw a hollow rectangular box.
     </li>

     <li><i>void draw_flood(struct image *i, int x, int y);</i><br />
     Flood-fill (dump the paint can) with the current color, starting at a particular location.
     </li>

     <li><i>void draw_write_jpeg(struct image *i, char *filename);</i><br />
     Write the current image to disk as a JPEG using the specified path and file name.
     </li>

     <li><i>void draw_write_png(struct image *i, char *filename);</i><br />
     Write the current image to disk as a PNG using the specified path and file name.
     </li>

     <li><i>void draw_write_tiff(struct image *i, char *filename);</i><br />
     Write the current image to disk as a TIFF using the specified path and file name.
     </li>

     <li><i>void draw_free_image(struct image *i);</i><br />
     Free up the image from dynamic memory. This should always be done when you're done with an image created with <i>draw_create_image()</i>.
     </li>

     </ul>
  </div>
  
  <div id="known_bugs">
     <h2>Known Bugs:</h2>
     <ol>
     <li><i>draw_flood()</i> needs to be re-written with a recursive algorithm. Some shapes aren't currently being filled in fully.</li>
     <li>The code currently produces unpredictable results if you try to draw off of the image canvas. This could be more elegant.</li>
     </ol>
  </div>

  <div>  
    <hr />
  </div>

<? include("../../fineprint.html") ?>

<? include("../../analytics.html"); ?>

</body>
</html>
