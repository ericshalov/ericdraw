/*
  convert.c
  A small example to demonstrate use of EricDraw library
  Converts a JPEG, PNG, or TIFF to another format

  Copyright (c) 2010, Eric Shalov
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of the <organization> nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ericdraw.h"

int main(int argc, char *argv[]) {
     char *input_filename, *output_filename;
     struct image *i;

     if(argc != 3) {
       fprintf(stderr,"Usage: %s [image-filename.jpg] [output-filename.png]\n", argv[0]);
       exit(1);
     }
     
     input_filename  = argv[1];
     output_filename = argv[2];

     if(strcmp(input_filename+strlen(input_filename)-4,".jpg")==0)
       i = draw_create_image_from_jpeg(input_filename);
     else if(strcmp(input_filename+strlen(input_filename)-5,".jpeg")==0)
       i = draw_create_image_from_jpeg(input_filename);
     else if(strcmp(input_filename+strlen(input_filename)-4,".tif")==0)
       i = draw_create_image_from_tiff(input_filename);
     else if(strcmp(input_filename+strlen(input_filename)-5,".tiff")==0)
       i = draw_create_image_from_tiff(input_filename);
     else if(strcmp(input_filename+strlen(input_filename)-4,".png")==0)
       i = draw_create_image_from_png(input_filename);
     else {
       fprintf(stderr,"Unrecognized file extension. Input file should end in .jpg, .tif, or ,png.\n");
       exit(1);
     }
     
     
     if(strcmp(output_filename+strlen(output_filename)-4,".jpg")==0)
       draw_write_jpeg(i, output_filename);
     else if(strcmp(output_filename+strlen(output_filename)-5,".jpeg")==0)
       draw_write_jpeg(i, output_filename);
     else if(strcmp(output_filename+strlen(output_filename)-4,".tif")==0)
       draw_write_tiff(i, output_filename);
     else if(strcmp(output_filename+strlen(output_filename)-5,".tiff")==0)
       draw_write_tiff(i, output_filename);
     else if(strcmp(output_filename+strlen(output_filename)-4,".png")==0)
       draw_write_png(i, output_filename);
     else {
       fprintf(stderr,"Unrecognized file extension. Output file should end in .jpg, .tif, or ,png.\n");
       exit(1);
     }

     draw_free_image(i);

     return 0;
}
