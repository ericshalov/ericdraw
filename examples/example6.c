/*
  example6.c
  A small example to demonstrate use of EricDraw library
  Generates a 300 x 300 PNG with a few geometrical objects on it.

  Copyright (c) 2010, Eric Shalov.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of the <organization> nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>

#include "ericdraw.h"

int main() {
     char filename[] = "example6.png";
     struct image *i;
     int x,y;
     double a, r;

     i = draw_create_image(300,300);

     draw_full(i, white);

     draw_color(i, black);
     for(x=-4;x<=4;x++) draw_textf(i, 150+x*30+3,150+3, 1, "%d", x);
     for(y=-4;y<=4;y++) draw_textf(i, 150+3,150-y*30+4, 1, "%d", y);

     draw_color(i,blue);
     for(x=0;x<300;x+=30) draw_line(i, x,0, x,i->height-1);
     for(y=0;y<300;y+=30) draw_line(i, 0,y, i->width-1,y);
     draw_box(i, 0,0, i->width-1,i->height-1);
     
     draw_color(i, black);

     for(x=-1;x<=1;x++)     
       draw_line(i, 150+x,0, 150+x,i->height-1);
     for(y=-1;y<=1;y++)     
       draw_line(i, 0,150+y, i->width-1,150+y);
     
     draw_color(i, green);
     draw_circle(i, 150,150, 90);
     
     /* Epitrochoid */
     for(a = 0.0; a < 360.0; a += 0.5) {
       r = 120.0 + sin(a/180*M_PI*3.0)*20.0;
       draw_pixel(i, 150+sin(a/180*M_PI)*r,150+cos(a/180*M_PI)*r, red);
     }

     draw_color(i, black);
     draw_textf(i, 5,i->height-8-2, 1, "Epitrochoid");

     draw_write_png(i, filename);

     draw_free_image(i);

     return 0;
}
