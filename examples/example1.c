/*
  example1.c
  A small example to demonstrate use of EricDraw library
  Generates a 640 x 480 PNG with a few geometrical objects on it.

  Copyright (c) 2010, Eric Shalov.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of the <organization> nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>

#include "ericdraw.h"

int main() {
     char filename[] = "example1.png";
     struct image *i;
     int x,y;

     i = draw_create_image(640,480);

     draw_full(i, white);

     draw_textf(i, 0, 0, 8, "Text $&%%");

     draw_color(i,red);
     draw_line(i, 0,0, i->width-1, i->height-1);
     draw_line(i, 0,i->height-1, i->width-1, 0);

     draw_box(i, 100,100,300,300);
     draw_circle(i,200,200, 50);
     draw_circle(i,200,200, 100);
     draw_circle(i,200,200, 150);
     draw_circle(i,200,200, 200);
     draw_flood(i,200,200);
     draw_flood(i,200,75);

     /* Checkerboard */
     for(x=400;x<i->width;x++)
          for(y=240;y<i->height;y++)
            draw_pixel(i, x,y, 
               (x/10)%2 ? ((y/10)%2 ? white : black) : ((y/10)%2 ? darkgreen : white)
     );

     draw_write_png(i, filename);

     draw_free_image(i);

     return 0;
}
