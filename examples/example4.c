/*
  example4.c
  A small example to demonstrate use of EricDraw library
     
  Generates a X x 16 JPEG with a text message specified on the command-line,
  with noise added to deter spammers from making automated OCR efforts.

  This could be used to frustrate the efforts of spammers that scrape
  websites for e-mail addresses.

  Copyright (c) 2010, Eric Shalov.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of the <organization> nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ericdraw.h"

int main(int argc, char *argv[]) {
     char filename[] = "example4.jpg";
     struct image *i;
     int n;
     struct color noise_pixel;

     if(argc != 2) {
       fprintf(stderr,"Usage; %s [\"text string\"]\n", argv[0]);
       return 1;
     }

     i = draw_create_image(strlen(argv[1])*8*2,8*2);

     draw_color(i,black);
     draw_textf(i, 0,0, 2, "%s", argv[1]);
     
     for(n=0 ; n<(40*strlen(argv[1])) ; n++) {
       noise_pixel.r = (unsigned char)(255.0*rand()/RAND_MAX);
       noise_pixel.g = (unsigned char)(255.0*rand()/RAND_MAX);
       noise_pixel.b = (unsigned char)(255.0*rand()/RAND_MAX);

       draw_pixel(i,
         (int)(1.0*i->width*rand()/RAND_MAX),
         (int)(1.0*i->height*rand()/RAND_MAX),
         noise_pixel
       );
     }
     
     draw_write_jpeg(i, filename);

     draw_free_image(i);

     return 0;
}
