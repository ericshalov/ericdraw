/*
  example7.c
  A small example to demonstrate use of EricDraw library
  Generates some geometrical shapes

  Copyright (c) 2010, Eric Shalov.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of the <organization> nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>

#include "ericdraw.h"

int main() {
     char filename[] = "example7.png";
     struct image *i;
     double a;
     int x1,x2,y1,y2;
     int sides;
     double r;

     i = draw_create_image(300,300);

     draw_full(i, white);

     draw_color(i,red);
     
     draw_circle(i, 150,150, 150);
     draw_circle(i, 150,150, 120);

     for(a=0.0 ; a<=360.0 ; a+=15.0) {
       x1 = 150 + (int)(sin(a/180.0*M_PI)*120.0);
       y1 = 150 - (int)(cos(a/180.0*M_PI)*120.0);
       x2 = 150 + (int)(sin(a/180.0*M_PI)*150.0);
       y2 = 150 - (int)(cos(a/180.0*M_PI)*150.0);
       draw_line(i, x1,y1, x2,y2);
     }

     draw_color(i, blue);
     for(sides=3;sides <= 12;sides+=3) {
       r = 20.0 + sides * 8;
       for(a=0.0 ; a<=360.0 ; a+=(360.0/sides)) {
         x1 = 150 + (int)(sin(a/180.0*M_PI)*r);
         y1 = 150 - (int)(cos(a/180.0*M_PI)*r);
         x2 = 150 + (int)(sin((a+360/sides)/180.0*M_PI)*r);
         y2 = 150 - (int)(cos((a+360/sides)/180.0*M_PI)*r);
         draw_line(i, x1,y1, x2,y2);
       }
     }

     draw_write_png(i, filename);

     draw_free_image(i);

     return 0;
}
