/*
  EricDraw
  A small JPEG-generating drawing/painting library based on libjpeg.

  Copyright (c) 2010-2014, Eric Shalov.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of the <organization> nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
          
#include <stdio.h>
#include <math.h>
#include <jpeglib.h>
#include <stdarg.h>

struct color {
  unsigned char r,g,b;
};

struct image {
  int width, height, components;
  unsigned char *buffer;
  struct color foreground, background;
};

#define PIXEL(i,x,y) (*(struct color *)&i->buffer[i->width * i->components * y + i->components * x])
#define SAME_COLOR(c1,c2) (c1.r==c2.r && c1.g==c2.g && c1.b==c2.b)

/* Standard colors */
struct color white      = {0xFF,0xFF,0xFF};
struct color black      = {0x00,0x00,0x00};
struct color red        = {0xFF,0x00,0x00};
struct color green      = {0x00,0xFF,0x00};
struct color blue       = {0x00,0x00,0xFF};
struct color darkred    = {0x7F,0x00,0x00};
struct color darkgreen  = {0x00,0x7F,0x00};
struct color darkblue   = {0x00,0x00,0x7F};
struct color cyan       = {0x00,0xFF,0xFF};
struct color yellow     = {0xFF,0xFF,0x00};
struct color magenta    = {0xFF,0x00,0xFF};
struct color gray       = {0x7F,0x7F,0x7F};
struct color orange     = {0xFF,0xA0,0x00};
struct color pink       = {0xB0,0x20,0xA0};

/* Functions that create a new image canvas */
struct image *draw_create_image(int width, int height);
struct image *draw_create_image_by_cropping(struct image *i, int x1,int y1,int x2,int y2);
struct image *draw_create_image_by_rotating(struct image *i, int x,int y, double degrees);
struct image *draw_create_image_from_tiff(char *filename);
struct image *draw_create_image_from_jpeg(char *filename);
struct image *draw_create_image_from_png(char *filename);

/* Functions to configure the current palette's drawing properties */
#define draw_color(i,color) (i->foreground = color)
#define draw_background_color(i,color) (i->background = color)

/* Functions to draw on the image canvas */
void draw_pixel(struct image *i, int x, int y, struct color color);
void draw_text(struct image *i, int x, int y, int scale, char *text);
void draw_textf(struct image *i, int x, int y, int scale, char *format, ...);
void draw_fill_block(struct image *i, int x1,int y1,int x2,int y2, struct color color);
#define draw_full(i, color) draw_fill_block(i,0,0,i->width-1,i->height-1, color)
void draw_line(struct image *i, int x1, int y1, int x2, int y2);
void draw_circle(struct image *i, int x, int y, double radius);
void draw_box(struct image *i, int x1, int y1, int x2, int y2);
void draw_flood(struct image *i, int x, int y);

/* Functions to save the image canvas to disk */
void draw_write_jpeg(struct image *i, char *filename);
void draw_write_tiff(struct image *i, char *filename);
void draw_write_png(struct image *i, char *filename);

/* Cleanup routine */
void draw_free_image(struct image *i);
