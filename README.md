ericdraw
========
EricDraw - A simple and tiny drawing/painting library written in C.

Intended as a much less-sophisticated replacement for libraries like
libgd, for applications where a tiny graphics library is useful (for
low-footprint CGI graphics generation, or in embedded development). 

![Elevation Model](examples/N33W119.jpg "Elevation Model")
![Geometry](examples/example7.png "Geometry")
![Cessna 182](examples/cessna2.png "Cessna 182")
![Epitrochoid](examples/example6.png "Epitrochoid")

Prerequisites
-------------
EricDraw Requires libjpeg, libtiff and libpng.

On Debian, install the necessary packages:
        # apt-get install libjpeg8-dev libpng12-dev libjbig0 libtiff5


Examples:

- Example 1: Drawing some lines and other stuff.
  You can draw lines, circles, boxes, text. You can flood-fill (paint-bucket) areas.
![example1.png](examples/example1.png "example1.png")

- Example 2: Drawing text
  A 7-bit ASCII font is included with the library. 
![example2.png](examples/example2.png "example2.png")
     
- Example 3: Text scaling
  The built-in rasterized font (8x8 pixels) can be scaled to add basic labels to your images.
![example3.png](examples/example3.png "example3.png")
     
- Example 4: Deterring spammers
  Deter spammers that use "screen-scraping" automated spiders to collect
  e-mail addresses off of websites. Encode e-mail addresses as images to
  make the task more difficult for them, and convolute the images (making them "captchas").
![example4.png](examples/example4.png "example4.png")
     
![eyes.jpg](examples/eyes.jpg "eyes.jpg")
     Example 5: Cropping an image
     The *draw_create_image_by_cropping()* function can be used to
     create a new image by cropping down an existant image canvas.
     
Downloads:
----------
Clone the repo:

$ git clone 'https://github.com/EricShalov/ericdraw.git'

Quickstart:
-----------
- Include the library header:
     <pre>#include "ericdraw.h"</pre>
     

- Declare a pointer to act as the handle for the image canvas:
     <pre>struct image *i;</pre>
     

- Create an image canvas *i* in memory with *draw_create_image(width,height)*
     <pre>i = draw_create_image(320,240);</pre>
     

- Draw on the image with *draw_line()*, etc..
     <pre>
     draw_color(i, black);
     draw_line(i, 160, 30,  120,210);
     draw_line(i, 160, 30,  200,210);
     draw_line(i,  80,120,  240,120);
     draw_box(i, 60,20, 260,220);
     draw_circle(i, 160,120, 70);
     </pre>
     
     
- Write the image to disk with *draw_write_png()*
     <pre>draw_write_png(i, "myimage.png");</pre>
     
     
- Clean-up by calling *draw_free_image()*
     <pre>draw_free_image(i);</pre>
     
          
- Link against the EricDraw library, libjpeg, libtiff, and libpng (you may also need the math library):
     <pre>$ <b>gcc -o *myprogram* *myprogram.c* -L. -lericdraw -lm -ljpeg -lpng -ltiff</b></pre>

     
Function Reference:
-------------------
- *struct image *draw_create_image(int width, int height);*
     Allocate (dynamically) an image of a given size. This must be done before
     calling any drawing functions. Use the image canvas handle that it returns
     in all future calls.  Don't forget to call *draw_free_image()* later.
     

- *struct image *draw_create_image_by_cropping(struct image *i, int x1,int y1,int x2,int y2);*
     Create a new image canvas by cropping an existing one. The original image
     remains unperturbed, and thus both the original image and the new one will
     need to be de-allocated with *draw_free_image()*.
     

- *struct image *draw_create_image_from_tiff(char *filename);*
     Create a new image by loading in a TIFF file from disk. Don't forget to call *draw_free_image()* later.
     

- *struct image *draw_create_image_from_png(char *filename);*
     Create a new image by loading in a PNG file from disk. Don't forget to call *draw_free_image()* later.
     

- *struct image *draw_create_image_from_jpeg(char *filename);*
     Create a new image by loading in a JPEG file from disk. Don't forget to call *draw_free_image()* later.
     
     
- *void draw_color(struct image *i, struct color color);*
     Set the current drawing (pen) color.
     

- *void draw_background_color(struct image *i, struct color color);*
     Set the current background color for text.
     

- *void draw_pixel(struct image *i, int x, int y, struct color color);*
     Set a single pixel to a particular color.
     

- *void draw_text(struct image *i, int x, int y, int scale, char *text);*
     Draw 7-bit ASCII text using a simple built-in raster font.
     

- *void draw_textf(struct image *i, int x, int y, int scale, char *format, ...);*
     Same as draw_text, but allows printf()-style formatting of any desired output.
     

- *void draw_fill_block(struct image *i, int x1,int y1,int x2,int y2, struct color color);*
     Fill a blocks solidly in the specified color.
     

- *void draw_full(struct image *i, struct color color);*
     Fill the ENTIRE image with a specified color (a good way to initialize the image.)
     

- *void draw_line(struct image *i, int x1, int y1, int x2, int y2);*
     Draw a 2D line.
     

- *void draw_circle(struct image *i, int x, int y, double radius);*
     Draw a circle.
     

- *void draw_box(struct image *i, int x1, int y1, int x2, int y2);*
     Draw a hollow rectangular box.
     

- *void draw_flood(struct image *i, int x, int y);*
     Flood-fill (dump the paint can) with the current color, starting at a particular location.
     

- *void draw_write_jpeg(struct image *i, char *filename);*
     Write the current image to disk as a JPEG using the specified path and file name.
     

- *void draw_write_png(struct image *i, char *filename);*
     Write the current image to disk as a PNG using the specified path and file name.
     

- *void draw_write_tiff(struct image *i, char *filename);*
  Write the current image to disk as a TIFF using the specified path and file name.
     

- *void draw_free_image(struct image *i);*
  Free up the image from dynamic memory. This should always be done when you're done with an image created with *draw_create_image()*.
  
Known Bugs:
-----------
- *draw_flood()* needs to be re-written with a recursive algorithm. Some shapes aren't currently being filled in fully.
- The code currently produces unpredictable results if you try to draw off of the image canvas. This could be more elegant.

License:
--------
EricDraw is licensed under the <a href="http://opensource.org/licenses/BSD-3-Clause">BSD 3-Clause License</a>.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the {organization} nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.
