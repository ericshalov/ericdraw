/*
  ericdraw.c
  Main guts of the EricDraw painting library

  Copyright (c) 2010, Eric Shalov
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of the <organization> nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*#define PNG_USER_MEM_SUPPORTED*/

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include "ericdraw.h"
#include "ericfont.h"
#include <libpng12/png.h>
#include <tiffio.h>

struct image *draw_create_image(int width, int height) {
  struct image *i;
  int x,y;
  int row_stride;                 /* physical row width in buffer */
  
  i = malloc( sizeof(struct image) );
  
  i->width = width;
  i->height = height;
  i->components = 3;
  i->buffer = malloc(i->width * i->height * i->components);
  i->foreground = black;
  i->background = white;

  row_stride = i->width * i->components;
  
  for(y=0 ; y < i->height ; y++) for(x=0;x < i->width ; x++) {
    i->buffer[row_stride*y + i->components*x + 0] = i->background.r;
    i->buffer[row_stride*y + i->components*x + 1] = i->background.g;
    i->buffer[row_stride*y + i->components*x + 2] = i->background.b;
  }

  
  return i;
}

struct image *draw_create_image_by_cropping(struct image *i, int x1,int y1,int x2,int y2) {
  int y;
  struct image *n;

  /* Bounds checking */
  if(x1 > i->width  || x1 <= 0 ||
     y1 > i->height || y1 <= 0 ||
     x2 > i->width  || x2 <= 0 ||
     y2 > i->height || y2 <= 0) return NULL;
  
  n = malloc( sizeof(struct image) );
  
  if(!n) fprintf(stderr,"Out of memory!\n");
  
  n->width  = x2 - x1 + 1;
  n->height = y2 - y1 + 1;
  n->components = i->components;
  n->foreground = i->foreground;
  n->background = i->background;
  n->buffer = malloc(n->width * n->height * n->components);

  for(y=y1;y<=y2;y++)
    memcpy(&n->buffer[((y-y1) * n->width     ) * n->components],
           &i->buffer[(   y   * i->width + x1) * n->components],
           n->width * i->components);

  return n;
}

struct image *draw_create_image_by_rotating(struct image *i, int x,int y, double degrees) {
  int xs,ys, xd,yd;
  struct image *n;

  /* Bounds checking */
  if(x > i->width  || x < 0 ||
     y > i->height || y < 0) return NULL;
  
  n = malloc( sizeof(struct image) );
  
  if(!n) fprintf(stderr,"Out of memory!\n");
  
  n->width  = i->width;
  n->height = i->height;
  n->components = i->components;
  n->foreground = i->foreground;
  n->background = i->background;
  n->buffer = malloc(n->width * n->height * n->components);
  
  for(yd=0;yd<i->height;yd++)
    for(xd=0;xd<i->width;xd++) {
      /*
        xs = xd + sin(M_PI * degrees/180.0) * 100.0;
        ys = yd + cos(M_PI * degrees/180.0) * 100.0;
      */

      xs = x + sin(M_PI * 15.0/180.0)*xd;
      ys = 0 + cos(M_PI * 15.0/180.0)*yd;
      
printf("dest[%3d,%3d] = src[%3d,%3d]\n", xd,yd, xs,ys);
      if(xs >= 0 && xs < i->width && ys >= 0 && ys < i->height)
        PIXEL(n,xd,yd) = PIXEL(i,xs,ys);
      else PIXEL(n,xd,yd) = white;
    }

  return n;
}

void draw_free_image(struct image *i) {
  free(i->buffer);
  free(i);
}

void draw_pixel(struct image *i, int x, int y, struct color color) {
  int row_stride;                 /* physical row width in buffer */
  
  row_stride = i->width * i->components;
  
  i->buffer[row_stride*y + i->components*x + 0] = color.r;
  i->buffer[row_stride*y + i->components*x + 1] = color.g;
  i->buffer[row_stride*y + i->components*x + 2] = color.b;
}

void draw_fill_block(struct image *i, int x1,int y1,int x2,int y2, struct color color) {
  int x,y;
  int row_stride;                 /* physical row width in buffer */
  
  row_stride = i->width * i->components;
  
  for(y=y1;y<=y2;y++) for(x=x1;x<=x2;x++) {
    i->buffer[row_stride*y + i->components*x + 0] = color.r;
    i->buffer[row_stride*y + i->components*x + 1] = color.g;
    i->buffer[row_stride*y + i->components*x + 2] = color.b;
  }
}

void draw_text(struct image *i, int x, int y, int scale, char *text) {
    int n;
    int h,v;
  
    for(n=0;text[n];n++) {
      for(h=0;h<8;h++) for(v=0;v<8;v++)
        if(scale == 1)
          draw_pixel(i, x+h+n*8, y+v, font[(unsigned char)text[n]][v*8+h] == 'X' ?
            i->foreground : i->background);
        else
        draw_fill_block(i,
          x + n*8*scale + h*scale,     y + v*scale,
          x + n*8*scale + h*scale + (scale-1), y + v*scale + (scale-1),
          
          (font[(unsigned char)text[n]][v*8+h]&0x7F) == 'X' ? i->foreground : i->background);
      
    }
}

void draw_textf(struct image *i, int x, int y, int scale, char *format, ...) {
  va_list arg;
  char *text = NULL;
  
  va_start(arg, format);
  vasprintf(&text, format, arg);
  va_end(arg);
  
  draw_text(i, x,y, scale, text);

  free(text);
}

void draw_line(struct image *i, int x1,int y1, int x2,int y2) {
  int x,y;
  int t;

  if(x1 == x2) {
    if(y2<y1) {
      t = y2;
      y2 = y1;
      y1 = t;
    }
    for(y=y1;y<=y2;y++) draw_pixel(i, x1,y, i->foreground);
  }

  else if(y1 == y2) {
    if(x2<x1) {
      t = x2;
      x2 = x1;
      x1 = t;
    }
    for(x=x1;x<=x2;x++) draw_pixel(i, x,y1, i->foreground);
  }
  
  else {
    if(x1 > x2) {
      t = x2;
      x2 = x1;
      x1 = t;
      
      t = y2;
      y2 = y1;
      y1 = t;
    }

    if( (x2-x1) > abs(y2-y1) ) {
      for(x=x1;x<=x2;x++) {
        y = 1.0*y1 + (1.0*(x-x1)/(x2-x1))*(y2-y1);
        draw_pixel(i, x,y, i->foreground);
      }
    } else {
      if(y2>y1)
        for(y=y1;y<=y2;y++) {
          x = 1.0*x1 + (1.0*(y-y1)/(y2-y1))*(x2-x1);
          draw_pixel(i, x,y, i->foreground);
        }
      else {
        for(y=y2;y<=y1;y++) {
          x = 1.0*x1 + (1.0*(y1-y)/(y1-y2))*(x2-x1);
          draw_pixel(i, x,y, i->foreground);
        }
      }
    }
  }
}

void draw_circle(struct image *i, int x, int y, double radius) {
  int pixel;
  int xp, yp;
  int pixels = 2.0*M_PI*radius;
  
  for(pixel=0;pixel<pixels;pixel++) {
    xp = x -  sin( 2.0 * pixel/pixels * M_PI)*radius;
    yp = y -  cos( 2.0 * pixel/pixels * M_PI)*radius;
    if(xp >= 0 && xp < i->width && yp >= 0 && yp < i->height)
      draw_pixel(i, xp,yp, i->foreground);
  }

}

void draw_box(struct image *i, int x1, int y1, int x2, int y2) {
  draw_line(i, x1,y1, x2,y1);
  draw_line(i, x2,y1, x2,y2);
  draw_line(i, x2,y2, x1,y2);
  draw_line(i, x1,y2, x1,y1);
}

void draw_flood(struct image *i, int x, int y) {
  int xf,yf;

  /* buggy: must recur in order to "creep around" areas, only good
     for "encircled" areas */

  /* flood down */  
  for(yf=y;yf<i->height;yf++) {
    /* flood left */
    for(xf=x-1;xf>0 && !SAME_COLOR(PIXEL(i,xf,yf),i->foreground);xf--)
      draw_pixel(i,xf,yf,i->foreground);
    /* flood right */
    for(xf=x;xf<i->width && !SAME_COLOR(PIXEL(i,xf,yf),i->foreground);xf++)
      draw_pixel(i,xf,yf,i->foreground);
    if(xf >= (x-1) && xf <= (x+1)) break;
  }

  /* flood up */  
  for(yf=(y-1);yf>0;yf--) {
    /* flood left */
    for(xf=x-1;xf>0 && !SAME_COLOR(PIXEL(i,xf,yf),i->foreground);xf--)
      draw_pixel(i,xf,yf,i->foreground);
    /* flood right */
    for(xf=x;xf<i->width && !SAME_COLOR(PIXEL(i,xf,yf),i->foreground);xf++)
      draw_pixel(i,xf,yf,i->foreground);
    if(xf >= (x-1) && xf <= (x+1)) break;
  }
}


void draw_write_jpeg(struct image *i, char *filename) {
     struct jpeg_compress_struct cinfo;
     struct jpeg_error_mgr jerr;
     JSAMPROW row_pointer[1];        /* pointer to a single row */
     int row_stride;                 /* physical row width in buffer */
     FILE *jpeg;

     if( ! (jpeg=fopen(filename,"w")) ) {
          fprintf(stderr,"Unable to open `%s': ", filename);
          perror(NULL);
     }
     
     else {
          cinfo.err = jpeg_std_error(&jerr);
          jpeg_create_compress(&cinfo);

          cinfo.image_width = i->width;      /* image width and height, in pixels */
          cinfo.image_height = i->height;
          cinfo.input_components = i->components;     /* # of color components per pixel */
          cinfo.in_color_space = JCS_RGB; /* colorspace of input image */

          row_stride = cinfo.image_width * i->components;   /* JSAMPLEs per row in image_buffer */
          
          jpeg_stdio_dest(&cinfo, jpeg);

          jpeg_set_defaults(&cinfo);

          /* Make optional parameter settings here */

          jpeg_start_compress(&cinfo, TRUE);
          while (cinfo.next_scanline < cinfo.image_height) {
               row_pointer[0] = &i->buffer[cinfo.next_scanline * row_stride];
               jpeg_write_scanlines(&cinfo, row_pointer, 1);
          }
          jpeg_finish_compress(&cinfo);
          
          fclose(jpeg);

          jpeg_destroy_compress(&cinfo);
     }
}

void donothing() {}

struct image *draw_create_image_from_tiff(char *filename) {
  struct image *i = NULL;
  size_t npixels;
  unsigned char *raster;
  int xd,yd;

  TIFFSetWarningHandler(donothing); /* suppress warnings */

  TIFF *tif = TIFFOpen(filename, "r");
  
  if (tif) {
    i = malloc(sizeof(struct image));

    TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &i->width);
    TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &i->height);
    i->components = 3;

    npixels = i->width * i->height;
    raster = (unsigned char *)_TIFFmalloc(npixels * sizeof (unsigned int));
    if (raster != NULL) {
      if(TIFFReadRGBAImage(tif, i->width, i->height, (unsigned int *)raster, 0)) {
        i->buffer = malloc(i->width * i->height * i->components);
        for(yd=0;yd<i->height;yd++) {
          for(xd=0;xd<i->width;xd++) {
            PIXEL(i,xd,yd).r = raster[((i->height-yd)*i->width+xd)*4+0];
            PIXEL(i,xd,yd).g = raster[((i->height-yd)*i->width+xd)*4+1];
            PIXEL(i,xd,yd).b = raster[((i->height-yd)*i->width+xd)*4+2];
          }
        }
      } else {
        free(i);
        i = NULL;
      }
      _TIFFfree(raster);
    }
    TIFFClose(tif);
  }
  
  return i;
}


struct image *draw_create_image_from_jpeg(char *filename) {
     struct jpeg_decompress_struct cinfo;
     struct jpeg_error_mgr jerr;
     JSAMPROW row_pointer[1];        /* pointer to a single row */
     int y;
                                
     FILE *jpeg;
     
     struct image *i = NULL;

     if( ! (jpeg=fopen(filename,"rb")) ) {
          fprintf(stderr,"Unable to open `%s': ", filename);
          perror(NULL);
     }
     
     else {
          cinfo.err = jpeg_std_error(&jerr);
          jpeg_create_decompress(&cinfo);

          jpeg_stdio_src(&cinfo, jpeg);

          jpeg_read_header(&cinfo, TRUE);

          if(cinfo.num_components != 3) {
            fprintf(stderr,"Sorry, can only read RGB JPEG's.\n");
            return NULL;
          }
          
          i = malloc(sizeof(struct image));
          i->width = cinfo.image_width;
          i->height = cinfo.image_height;
          i->components = cinfo.num_components;

          i->buffer = malloc(i->width * i->height * i->components);

          jpeg_start_decompress(&cinfo);

          for(y=0;y<i->height;y++) {
            row_pointer[0] = &i->buffer[y * i->width * i->components];
            jpeg_read_scanlines(&cinfo, row_pointer, 1);
          }

          jpeg_finish_decompress(&cinfo);

          fclose(jpeg);

          jpeg_destroy_decompress(&cinfo);
     }
     
     return i;
}

void draw_write_tiff(struct image *i, char *filename) {
  unsigned char *raster;
  int xd,yd;

  TIFFSetWarningHandler(donothing); /* suppress warnings */

  TIFF *tif = TIFFOpen(filename, "w");

  TIFFSetField(tif, TIFFTAG_IMAGEWIDTH,  i->width);
  TIFFSetField(tif, TIFFTAG_IMAGELENGTH, i->height);
  TIFFSetField(tif, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
  TIFFSetField(tif, TIFFTAG_RESOLUTIONUNIT, (int)2);
  TIFFSetField(tif, TIFFTAG_XRESOLUTION, (float) 72.0);
  TIFFSetField(tif, TIFFTAG_YRESOLUTION, (float) 72.0);
  TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 3);
  TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE,   8);
  TIFFSetField(tif, TIFFTAG_PHOTOMETRIC,     PHOTOMETRIC_RGB);
  TIFFSetField(tif, TIFFTAG_PLANARCONFIG,    PLANARCONFIG_CONTIG);

  TIFFSetField(tif, TIFFTAG_COMPRESSION,     COMPRESSION_LZW);
/*  TIFFSetField(tif, TIFFTAG_COMPRESSION,     COMPRESSION_JPEG);*/
  
                           
  if (tif) {
    raster = malloc(i->width * 3);
    if (raster != NULL) {
      for(yd=0;yd<i->height;yd++) {
        for(xd=0;xd<i->width;xd++) {
          raster[xd*3+0] = PIXEL(i,xd,yd).r;
          raster[xd*3+1] = PIXEL(i,xd,yd).g;
          raster[xd*3+2] = PIXEL(i,xd,yd).b;
        }
        TIFFWriteScanline(tif,raster,yd,0);
      }
      free(raster);
    }
    TIFFClose(tif);
  }
}

void draw_write_png(struct image *i, char *filename) {
     FILE *png;
     png_structp png_ptr;
     png_infop info_ptr;
     png_voidp user_error_ptr;
     png_byte **row_pointers;
     int y;
         
          
     if( ! (png=fopen(filename,"w")) ) {
          fprintf(stderr,"Unable to open `%s': ", filename);
          perror(NULL);
     }
     
     else {
          
          /* png_struct and png_info need to be allocated and initialized */
          png_ptr = png_create_write_struct
               (PNG_LIBPNG_VER_STRING, (png_voidp)user_error_ptr, NULL, NULL);
          if (!png_ptr) return;

          info_ptr = png_create_info_struct(png_ptr);
          if (!info_ptr) {
               png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
               return;
          }
          
          png_init_io(png_ptr, png);
          
          png_set_filter(png_ptr, 0, PNG_FILTER_NONE);

          /* set the zlib compression level */
          png_set_compression_level(png_ptr, Z_BEST_COMPRESSION);

          png_set_IHDR(png_ptr, info_ptr, i->width, i->height,
               8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
               PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT
          );

          png_write_info(png_ptr, info_ptr);

          png_set_packswap(png_ptr);
                 
          row_pointers = malloc(i->height * sizeof(png_byte *));
          for(y=0;y<i->height;y++) row_pointers[y] = &i->buffer[y * i->width * i->components];
          png_write_image(png_ptr, row_pointers);
          
          png_write_flush(png_ptr);
          png_write_end(png_ptr, info_ptr);

          png_destroy_write_struct(&png_ptr, &info_ptr);

          free(row_pointers);
         
          fclose(png);
     }
}

struct image *draw_create_image_from_png(char *filename) {
  FILE *f;
  unsigned char header[8];
  png_byte **row_pointers;
  png_structp png_ptr;
  png_infop info_ptr;
  png_voidp user_error_ptr;
  int color_type, interlace_type, compression_type, filter_method;
  int y;
  
  struct image *i;
  
  if( (f=fopen(filename,"r")) ) {
    fread(header, 1, 8, f);
    if(png_sig_cmp(header, 0, 8) != 0) {
      fprintf(stderr,"File `%s' is not a valid PNG.\n", filename);
      return NULL;
    }
    
    /* png_struct and png_info need to be allocated and initialized */
    png_ptr = png_create_read_struct
         (PNG_LIBPNG_VER_STRING, (png_voidp)user_error_ptr, NULL, NULL);
    if (!png_ptr) return NULL;

    info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr) {
         png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
         return NULL;
    }

    png_init_io(png_ptr, f);
    
    png_set_sig_bytes(png_ptr, 8); /* tell libpng that we read in 8 bytes of header */

    png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);

    i = malloc(sizeof(struct image));    

    png_get_IHDR(png_ptr, info_ptr, (unsigned long *)&i->width, (unsigned long *)&i->height,
      &i->components, &color_type, &interlace_type,
      &compression_type, &filter_method);
    i->components = (i->components>>3) * 3;
    i->foreground = black;
    i->background = white;
                      
    if(color_type != PNG_COLOR_TYPE_RGB) {
      fprintf(stderr,"Sorry, can only handle RGB PNG's at this time.\n");
      return NULL;
    }

    i->buffer = malloc(i->width * i->height * i->components);
    if(! i->buffer) {
      fprintf(stderr,"Out of memory!\n");
      return NULL;
    }

    row_pointers = png_get_rows(png_ptr, info_ptr);
    for(y=0 ; y < i->height ; y++)
      memcpy(&i->buffer[y * i->width * i->components], row_pointers[y], i->width * i->components);

    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);

    fclose(f);

    return i;
    
  } else {
    fprintf(stderr,"Unable to open `%s': ", filename);
    perror(NULL);
    return NULL;
  }
}
