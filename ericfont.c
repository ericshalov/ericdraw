/*
      ericfont.c
      A simple 7-bit ASCII font for use with EricDraw
     
      Copyright (c) 2010-2014, Eric Shalov.
      All rights reserved.

      Redistribution and use in source and binary forms, with or without
      modification, are permitted provided that the following conditions are met:
          * Redistributions of source code must retain the above copyright
            notice, this list of conditions and the following disclaimer.
          * Redistributions in binary form must reproduce the above copyright
            notice, this list of conditions and the following disclaimer in the
            documentation and/or other materials provided with the distribution.
          * Neither the name of the <organization> nor the
            names of its contributors may be used to endorse or promote products
            derived from this software without specific prior written permission.

      THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
      ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
      WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
      DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
      DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
      (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
      LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
      ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
      (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
      SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.fg
*/

char font[128][64+1] = {
"        "
"        "
"        "
"        "
"        "
"        "
"        "
"        ",

" XXXXX  "
"X     X "
"X X X X "
"X     X "
"X XXX X "
"X     X "
" XXXXX  "
"        ",

" XXXXX  "
"XXXXXXX "
"XX X XX "
"XXXXXXX "
"XX   XX "
"XXXXXXX "
" XXXXX  "
"        ",

"        "
" XX XX  "
"XXXXXXX "
"XXXXXXX "
" XXXXX  "
"  XXX   "
"   X    "
"        ",

"   X    "
"  XXX   "
" XXXXX  "
"XXXXXXX "
" XXXXX  "
"  XXX   "
"   X    "
"        ",

"  XXX   "
"X  X  X "
"XXXXXXX "
"X  X  X "
"  XXX   "
"   X    "
"  XXX   "
"        ",

"   X    "
"  XXX   "
" XXXXX  "
"XXXXXXX "
" XXXXX  "
"   X    "
" XXXXX  "
"        ",

"        "
"        "
"  XXX   "
" XXXXX  "
"  XXX   "
"        "
"        "
"        ",

"XXXXXXX "
"XXXXXXX "
"XX   XX "
"X     X "
"XX   XX "
"XXXXXXX "
"XXXXXXX "
"        ",
"        "
"        "
"  XXX   "
" XX XX  "
"  XXX   "
"        "
"        "
"        ",

"XXXXXXX "
"XXXXXXX "
"XX   XX "
"X  X  X "
"XX   XX "
"XXXXXXX "
"XXXXXXX "
"        ",
 
"    XXX "
"     XX "
"    X X "
"   X    "
"  XXX   "
" X   X  "
"  XXX   "
"        ",

"  XXX   "
" X   X  "
"  XXX   "
"   X    "
" XXXXX  "
"   X    "
"   X    "
"        ",

"  XXXX  "
"  X  X  "
"  XXXX  "
"  X     "
" XX     "
"XXX     "
" XX     "
"        ",

"  XXXXX "
"  X   X "
"  XXXXX "
"  X   X "
" XX  XX "
"XXX XXX "
" XX  XX "
"        ",

" X X X  "
"  XXX   "
" X X X  "
" XXXXX  "
" X X X  "
"  XXX   "
" X X X  "
"        ",

"  XX    "
"  XXX   "
"  XXXX  "
"  XXXXX "
"  XXXX  "
"  XXX   "
"  XX    "
"        ",

"     XX "
"    XXX "
"   XXXX "
"  XXXXX "
"   XXXX "
"    XXX "
"     XX "
"        ",

"  XX    "
" XXXX   "
"XXXXXX  "
"  XX    "
"XXXXXX  "
" XXXX   "
"  XX    "
"        ",

" X X    "
" X X    "
" X X    "
" X X    "
" X X    "
"        "
" X X    "
"        ",

"  XXXXX "
" X  X X "
" X  X X "
"  XXX X "
"    X X "
"    X X "
"    X X "
"        ",

"  XXX   "
" XX     "
"  XXX   "
"  X X   "
"  XXX   "
"     XX "
"   XXX  "
"        ",

"        "
"        "
"        "
"        "
"        "
"XXXXXXX "
"XXXXXXX "
"        ",

"   X    "
"  XXX   "
" X X X  "
"   X    "
" X X X  "
"  XXX   "
"   X    "
"XXXXXXX ",

"   X    "
"  XXX   "
" X X X  "
"   X    "
"   X    "
"   X    "
"   X    "
"        ",

"   X    "
"   X    "
"   X    "
"   X    "
" X X X  "
"  XXX   "
"   X    "
"        ",

"        "
"  XX    "
"   XX   "
"XXXXXX  "
"   XX   "
"  XX    "
"        "
"        ",

"        "
"  XX    "
" XX     "
"XXXXXX  "
" XX     "
"  XX    "
"        "
"        ",

"        "
"        "
"X       "
"X       "
"XXXXXX  "
"        "
"        "
"        ",

"        "
"        "
" X   X  "
"XXXXXXX "
" X   X  "
"        "
"        "
"        ",

"        "
"        "
"   X    "
"  XXX   "
" XXXXX  "
"XXXXXXX "
"        "
"        ",

"        "
"        "
"XXXXXXX "
" XXXXX  "
"  XXX   "
"   X    "
"        "
"        ",

"        "
"        "
"        "
"        "
"        "
"        "
"        "
"        ",

"   X    "
"   X    "
"   X    "
"   X    "
"   X    "
"        "
"   X    "
"        ",

"  X X   "
"  X X   "
"        "
"        "
"        "
"        "
"        "
"        ",

"  X  X  "
" XXXXXX "
"  X  X  "
"  X  X  "
" XXXXXX "
"  X  X  "
"        "
"        ",

" XXXXX  "
"X  X    "
"X  X    "
" XXXXX  "
"   X  X "
"   X  X "
" XXXXX  "
"   X    ",

"  XX    "
" X  X X "
"  XX X  "
"    X   "
"   X    "
"  X XX  "
" X X  X "
"X   XX  ",

" XXX    "
"X   X   "
" X X    "
"  X     "
" X X X  "
"X   X   "
" XXX X  "
"        ",

"   X    "
"   X    "
"        "
"        "
"        "
"        "
"        "
"        ",

"   X    "
" XX     "
"X       "
"X       "
"X       "
" XX     "
"   X    "
"        ",

"   X    "
"    XX  "
"      X "
"      X "
"      X "
"    XX  "
"   X    "
"        ",

"   X    "
" X X X  "
"  XXX   "
"   X    "
"  XXX   "
" X X X  "
"   X    "
"        ",

"        "
"   X    "
"   X    "
" XXXXX  "
"   X    "
"   X    "
"        "
"        ",

"        "
"        "
"        "
"        "
"        "
"   X    "
"   XX   "
"    X   ",

"        "
"        "
"        "
"XXXXXXX "
"        "
"        "
"        "
"        ",

"        "
"        "
"        "
"        "
"        "
"        "
"X       "
"        ",

"      X "
"     X  "
"    X   "
"   X    "
"  X     "
" X      "
"X       "
"        ",

"  XXX   "
" X   X  "
"X   X X "
"X  X  X "
"X X   X "
" X   X  "
"  XXX   "
"        ",

"   X    "
"  XX    "
" X X    "
"   X    "
"   X    "
"   X    "
" XXXXX  "
"        ",

" XXXXX  "
"X     X "
"     XX "
"   XX   "
"  X     "
" X      "
"XXXXXXX "
"        ",

" XXXXX  "
"X     X "
"      X "
"   XXX  "
"      X "
"X     X "
" XXXXX  "
"        ",

"   XXXX "
"  X   X "
" X    X "
"XXXXXXX "
"      X "
"      X "
"      X "
"        ",

"XXXXXXX "
"X       "
"X       "
"XXXXXX  "
"      X "
"X     X "
" XXXXX  "
"        ",

" XXXXX  "
"X       "
"X       "
"XXXXXX  "
"X     X "
"X     X "
" XXXXX  "
"        ",

"XXXXXXX "
"      X "
"     X  "
"    X   "
"   X    "
"  X     "
" X      "
"        ",

" XXXXX  "
"X     X "
"X     X "
" XXXXX  "
"X     X "
"X     X "
" XXXXX  "
"        ",

" XXXXX  "
"X     X "
"X     X "
" XXXXX  "
"      X "
"      X "
" XXXXX  "
"        ",
 
"        "
"  X     "
"        "
"        "
"        "
"  X     "
"        "
"        ",

"        "
"  X     "
"        "
"        "
"        "
"  XX    "
"   X    "
"        ",

"    XX  "
"   XX   "
"  XX    "
" XX     "
"  XX    "
"   XX   "
"    XX  "
"        ",

"        "
"        "
"XXXXXXX "
"        "
"XXXXXXX "
"        "
"        "
"        ",

"XX      "
" XX     "
"  XX    "
"   XX   "
"  XX    "
" XX     "
"XX      "
"        ",

" XXXXX  "
"X     X "
"X    XX "
"    XX  "
"        "
"   XX   "
"   XX   "
"        ",

" XXXXX  "
"X     X "
"X XXX X "
"X X X X "
"X XXXXX "
"X       "
" XXXXX  "
"        ",

"   X    "
"  X X   "
" X   X  "
"X     X "
"XXXXXXX "
"X     X "
"X     X "
"        ",

"XXXXXX  "
"X     X "
"X     X "
"XXXXXX  "
"X     X "
"X     X "
"XXXXXX  "
"        ",

" XXXXX  "
"X     X "
"X       "
"X       "
"X       "
"X     X "
" XXXXX  "
"        ",

"XXXXXX  "
"X     X "
"X     X "
"X     X "
"X     X "
"X     X "
"XXXXXX  "
"        ",

"XXXXXXX "
"X       "
"X       "
"XXXXXXX "
"X       "
"X       "
"XXXXXXX "
"        ",

"XXXXXXX "
"X       "
"X       "
"XXXXXXX "
"X       "
"X       "
"X       "
"        ",

" XXXXX  "
"X     X "
"X       "
"X  XXX  "
"X     X "
"X     X "
" XXXXX  "
"        ",

"X     X "
"X     X "
"X     X "
"XXXXXXX "
"X     X "
"X     X "
"X     X "
"        ",

"XXXXXXX "
"   X    "
"   X    "
"   X    "
"   X    "
"   X    "
"XXXXXXX "
"        ",

"XXXXXXX "
"   X    "
"   X    "
"   X    "
"   X    "
"X  X    "
" XX     "
"        ",

"X    X  "
"X   X   "
"X  X    "
"X XX    "
"XX  X   "
"X    X  "
"X     X "
"        ",

"X       "
"X       "
"X       "
"X       "
"X       "
"X       "
"XXXXXXX "
"        ",

"XX   XX "
"X X X X "
"X  X  X "
"X  X  X "
"X     X "
"X     X "
"X     X "
"        ",

"X     X "
"XXX   X "
"X XX  X "
"X  XX X "
"X   XXX "
"X    XX "
"X     X "
"        ",

"  XXX   "
" X   X  "
"X     X "
"X     X "
"X     X "
" X   X  "
"  XXX   "
"        ",

"XXXXXX  "
"X     X "
"X     X "
"XXXXXX  "
"X       "
"X       "
"X       "
"        ",

" XXXXX  "
"X     X "
"X     X "
"X     X "
"X   X X "
"X    X  "
" XXXX X "
"        ",

"XXXXXX  "
"X     X "
"X     X "
"XXXXXX  "
"X     X "
"X     X "
"X     X "
"        ",

" XXXXX  "
"X     X "
"X       "
" XXXXX  "
"      X "
"X     X "
" XXXXX  "
"        ",

"XXXXXXX "
"   X    "
"   X    "
"   X    "
"   X    "
"   X    "
"   X    "
"        ",

"X     X "
"X     X "
"X     X "
"X     X "
"X     X "
"X     X "
" XXXXX  "
"        ",

"X     X "
"X     X "
"X     X "
"X     X "
" X   X  "
"  X X   "
"   X    "
"        ",

"X     X "
"X     X "
"X  X  X "
"X  X  X "
"X  X  X "
"X  X  X "
" XX XX  "
"        ",

"X     X "
" X   X  "
"  X X   "
"   X    "
"  X X   "
" X   X  "
"X     X "
"        ",

"X     X "
"X     X "
" X   X  "
"  X X   "
"   X    "
"   X    "
"   X    "
"        ",

/* Z */
"XXXXXXX "
"     X  "
"    X   "
"   X    "
"  X     "
" X      "
"XXXXXXX "
"        ",

" XXXXX  "
" X      "
" X      "
" X      "
" X      "
" X      "
" XXXXX  "
"        ",

"X       "
" X      "
"  X     "
"   X    "
"    X   "
"     X  "
"      X "
"        ",

" XXXXX  "
"     X  "
"     X  "
"     X  "
"     X  "
"     X  "
" XXXXX  "
"        ",

"   X    "
"  X X   "
" X   X  "
"        "
"        "
"        "
"        "
"        ",

"        "
"        "
"        "
"        "
"        "
"        "
"        "
"XXXXXXXX",

"  XX    "
"   XX   "
"        "
"        "
"        "
"        "
"        "
"        ",

"        "
"        "
"  XXX   "
" X   X  "
"X     X " 
" X   XX "
"  XXX X " 
"        ",

"X       "
"X       "  
"X XXX   "
"XX   X  "
"X     X "
"X     X "  
"XXXXXX  "
"        ",

"        "
"        "
" XXXXX  "
"X     X "
"X       "
"X     X "
" XXXXX  "
"        ",

"      X "
"      X "
"  XXX X "
"XX   XX "
"X     X "
"X     X "
" XXXXXX "
"        ",

"        "
"        "
" XXXXX  "
"X     X "
"XXXXXX  "
"X       "
" XXXXX  "
"        ",

"  XXXX  "
" X    X "
" X      "
"XXXXXX  "
" X      "
" X      "
" X      "
"        ",

"        "
"        "
"  XXXX  "
" X    X "
"  XXXXX "
"      X "
" X   X  "
"  XXX   ",

"X       "
"X       "
"X       "
"X XXX   "
"XX   X  "
"X     X "
"X     X "
"        ",

"        "
"  X     "
"        "
"  X     "
"  X     "
"  X     "
"  X     "
"        ",

"        "
"   X    "
"        "
"   X    "
"   X    "
"X  X    "
"X  X    "
" XX     ",

"        "
"X       "
"X  X    "
"X X     "
"XXX     "
"X  X    "
"X   X   "
"        ",

" X      "
" X      "
" X      "
" X      "
" X      "
" X      "
" XX     "
"        ",

"        "
"        "
"X XXXX  "
"XX X XX "
"X  X  X "
"X  X  X "
"X  X  X " 
"        ",

"        "
"        "
"X XXX   "
"XX  XX  "
"X    X  "
"X    X  "
"X    X  "
"        ",

"        "
"        "
" XXXX   "
"X    X  "
"X    X  "
"X    X  "
" XXXX   "
"        ",

"        "
"X       "
"X XX    "
"XX  X   "
"X   X   "
"XXXX    "
"X       "
"X       ",

"        "
"        "
"  XXXX  "
" X   X  "
"X    X  "
"X    X  "
" XXXXX  "
"     X  ",

"        "
"X       "
"XXXXX   "
"X    X  "
"X       "
"X       "
"X       "
"        ",

"        "
"        "
" XXXXX  "
"X       "
" XXXXX  "
"      X "
" XXXXX  "
"        ",

" X      "
" X      "
" X      "
"XXXX    "
" X      "
" X   X  "
"  XXX   "
"        ",

"        "
"        "
"X    X  "
"X    X  "
"X    X  "
"X   XX  "
" XXX X  "
"        ",

"        "
"        "
"X   X   "
"X   X   "
"X   X   "
" X X    "
"  X     "
"        ",

"        "
"        "
"X     X "
"X  X  X "
"X  X  X "
"X  X  X "
" XX XX  "
"        ",

"        "
"        "
"X   X   "
" X X    "
"  X     "
" X X    "
"X   X   "
"        ",

"        "
"        "
" X   X  "
" X   X  "
"  XXXX  "
"     X  "
" X   X  "
"  XXX   ",

"        "
"        "
"XXXXX   "
"   X    "
"  X     "
" X      "
"XXXXX   "
"        ",

"  XX    "
" X      "
" X      "
"X       "
" X      "
" X      "
"  XX    "
"        ",

"  XX    "
"  XX    "
"  XX    "
"  XX    "
"  XX    "
"  XX    "
"  XX    "
"        ",

" XX     "
"   X    "
"   X    "
"    X   "
"   X    "
"   X    "
" XX     "
"        ",

"        "
"        "
" XX     "
"X  X  X "
"    XX  "
"        "
"        "
"        ",

"   X    "
"  X X   "
" X   X  "
"X     X "
"X     X "
"XXXXXXX "
"        "
"        "

};
